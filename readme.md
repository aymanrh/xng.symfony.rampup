## Blog Website - using Symfony4.4

Please run the following Symfony commands to set up the project on your local machine:

- First, You will need to add your database configuration at **.env** or **.env.local** file then run the following command:
`bin/console doctrine:database:create`

- Then we will create the user, blog, category, tag, and other relation tables by running the following command: `bin/console doctrine:migrations:migrate`