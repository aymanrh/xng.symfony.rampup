<?php

namespace App\DataFixtures;

use App\Xngage\CategoryBundle\Factory\CategoryFactory;
use App\Xngage\TagBundle\Factory\TagFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        CategoryFactory::new()->createMany(10);
        TagFactory::new()->createMany(20);

        $manager->flush();
    }
}
