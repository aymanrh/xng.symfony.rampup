<?php

namespace App\Xngage\BlogBundle\Controller;

use App\Xngage\TagBundle\Entity\Tag;
use App\Xngage\BlogBundle\Form\BlogType;
use App\Xngage\BlogBundle\Entity\Blog;
use App\Xngage\BlogBundle\Repository\BlogRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/blog")
 */
class BlogController extends AbstractController
{
    /**
     * @Route("/", name="blog_index", methods={"GET"})
     * @Security("is_granted('ROLE_USER')")
     */
    public function index(BlogRepository $blogRepository): Response
    {
        return $this->render('@XngageBlog/Blog/index.html.twig', [
            'blogs' => $blogRepository->findByAuthor($this->getUser()),
        ]);
    }

    /**
     * @Route("/new", name="blog_new", methods={"GET","POST"})
     * @Security("is_granted('ROLE_USER')")
     */
    public function new(Request $request): Response
    {
        $blog = new Blog();
        $form = $this->createForm(BlogType::class, $blog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $blog->setAuthor($this->getUser());
            $entityManager->persist($blog);
            $entityManager->flush();

            return $this->redirectToRoute('blog_index');
        }

        return $this->render('@XngageBlog/Blog/new.html.twig', [
            'blog' => $blog,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id<\d+>}", name="blog_show", methods={"GET"})
     * @Security("is_granted('ROLE_USER') and blog.isAuthor(user)")
     */
    public function show(Blog $blog): Response
    {
        return $this->render('@XngageBlog/Blog/show.html.twig', [
            'blog' => $blog,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="blog_edit", methods={"GET","POST"})
     * @Security("is_granted('ROLE_USER')  and blog.isAuthor(user)")
     */
    public function edit(Request $request, Blog $blog): Response
    {
        $form = $this->createForm(BlogType::class, $blog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('blog_index');
        }

        return $this->render('@XngageBlog/Blog/edit.html.twig', [
            'blog' => $blog,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="blog_delete", methods={"POST"})
     * @Security("is_granted('ROLE_USER') and blog.isAuthor(user)")
     */
    public function delete(Request $request, Blog $blog): Response
    {
        if ($this->isCsrfTokenValid('delete'.$blog->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($blog);
            $entityManager->flush();
        }

        return $this->redirectToRoute('blog_index');
    }

    /**
     * @Route("/{slug}", name="blog_display", methods={"GET"})
     */
    public function displayBlog(Blog $blog): Response
    {
        return $this->render('@XngageBlog/Blog/show.html.twig', [
            'blog' => $blog,
        ]);
    }

    /**
     * @Route("/tag/{url}", name="tag_blogs", methods={"GET"})
     */
    public function displayTagBlogs(Tag $tag): Response
    {
        return $this->render('@XngageBlog/Blog/display.tag.blogs.html.twig', [
            'blog' => $tag,
        ]);
    }
}
