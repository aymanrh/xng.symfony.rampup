<?php

namespace App\Xngage\BlogBundle\Form;

use App\Xngage\BlogBundle\Entity\Blog;
use App\Xngage\CategoryBundle\Entity\Category;
use App\Xngage\CategoryBundle\Repository\CategoryRepository;
use App\Xngage\TagBundle\Entity\Tag;
use App\Xngage\TagBundle\Repository\TagRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BlogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('content')
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'query_builder' => function (CategoryRepository $categoryRepository) {
                    return $categoryRepository->createQueryBuilder('category')
                        ->orderBy('category.name', 'ASC');
                },
                'choice_value' => 'id',
                'choice_label' => function (?Category $category) {
                    return $category ? strtoupper($category->getName()) : '';
                },
                'preferred_choices' => function (?Category $category) {
                    return $category && 100 < $category->getBlogs()->count();
                },

            ])
            ->add('tags', EntityType::class, [
                'multiple' => true,
                'class' => Tag::class,
                'query_builder' => function (TagRepository $tagRepository) {
                    return $tagRepository->createQueryBuilder('tag')
                        ->orderBy('tag.label', 'ASC');
                },
                'choice_value' => 'id',
                'choice_label' => 'label'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Blog::class,
        ]);
    }
}
