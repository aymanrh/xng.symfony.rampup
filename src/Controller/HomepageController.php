<?php


namespace App\Controller;


use App\Xngage\BlogBundle\Repository\BlogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends  AbstractController
{
    /**
     * @Route("/", name="app_homepage", methods={"GET", "HEAD"})
     */
    public function index(BlogRepository $blogRepository): Response
    {
        return $this->render('homepage/index.html.twig', [
            'blogs' => $blogRepository->findAll(),
        ]);
    }
}